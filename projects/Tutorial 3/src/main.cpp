//Nathan Tuck 100708651
#include <iostream>

#include "Logging.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <GLM/glm.hpp>
GLFWwindow* window;

int main()
{
	Logger::Init();

	glfwInit();

	window = glfwCreateWindow(800, 600, "CG Intro", NULL, NULL);
	glfwMakeContextCurrent(window);

	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		LOG_ERROR("Failed to initialize glad");
		return 2;
	}

	//Triangle data
	static const GLfloat points1[] = {
		-1.0f, -0.8f, 0.0f,
		-0.4f, 1.f, 0.0f,
		0.0f, -0.4f, 0.0f,
		0.1f, 0.7f, 0.0f,
		0.9f, -0.9f, 0.0f,
		0.0f, -0.9f, 0.0f
	};


	//Vertex Buffer Object (VBO)
	GLuint pos_vbo1 = 0;
	glGenBuffers(1, &pos_vbo1);
	glBindBuffer(GL_ARRAY_BUFFER, pos_vbo1);
	glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), points1, GL_STATIC_DRAW);

	// VAO
	GLuint vao1 = 0;
	glGenVertexArrays(1, &vao1);
	glBindVertexArray(vao1);
	glBindBuffer(GL_ARRAY_BUFFER, pos_vbo1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);


	glEnableVertexAttribArray(0);

	// Vertex Shader
	const char* vertex_shader = "#version 400\n" "in vec3 vpos;" "void main() {" "	gl_Position = vec4(vpos, 1.0);" "}";

	// Fragment Shader
	const char* fragment_shader = "#version 400\n" "out vec4 frag_color;" "void main() {" "frag_color = vec4(1.0, 0.0, 0.5, 1.0); " "}";

	// Load Shaders
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);

	GLuint shader_program = glCreateProgram();
	glAttachShader(shader_program, vs);
	glAttachShader(shader_program, fs);
	glLinkProgram(shader_program);

	//printf("%d", glGetError());

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		glClearColor(0.0f, 0.8f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shader_program);
		glBindVertexArray(vao1);

		//draw vertices
		glDrawArrays(GL_TRIANGLES, 0, 6);


		glfwSwapBuffers(window);
	}



	return 0;
}
